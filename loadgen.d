/*
 * Synthetic CPU load generator.
 *
 * Author: Andrey Gelman <andrey.gelman@datarcs.com>
 *
 * This software is written in the D programming language [www.dlang.org].
 * Build command line:
 * dmd loadgen.d -ofloadgen
 */

import std.stdio;
import std.random;
import std.concurrency;
import std.array;
import std.conv;
import std.algorithm;
import std.experimental.logger;

import core.time;
import core.stdc.stdlib;


enum SIGINT  = 2;
enum SIGALRM = 14;

extern (C) void signal(int signum, void function(int));
extern (C) uint alarm(uint seconds);

extern (C) int usleep(uint usec);

/* Ref: www.gnu.org/software/libc/manual/html_node/Processor-Resources.html */
extern (C) int get_nprocs();

/*
 * core.stdc.signal implementation of signals does not allow
 * throwing an exception, so we prefer this version
 */
extern (C)
void sighandler(int signo)
{
	switch (signo) {
	case SIGINT:
		throw new Exception("Ctrl-C");

	case SIGALRM:
		throw new Exception("Timeout");

	default:
		break;
	}
}


enum DefaultTcpPort = 8008;
enum ArraySize = (1024 * 1024);


int[ArraySize / int.sizeof] iNumbers;
int iAns;

string helpText = "Usage: loadgen [OPTION [OPTION [...]]] \n" ~
		  "Synthetic CPU load generator for DatArcs Optimizer \n\n" ~
		  "Command line options: \n" ~
		  "  --num-threads=N  number of threads \n" ~
		  "  --timeout=T      exit after T seconds \n" ~
		  "  --tcp-port=P     TCP port for performance knobs \n" ~
		  "  --verbose        trace accepted commands \n" ~
		  "  --help           display this help and exit \n";


void main(string[] args)
{
	/* process command line */
	uint timeout = 0;
	uint numThreads = 0;
	ushort portno = DefaultTcpPort;
	bool verbose = false;

	for (int i = 1; i < args.length; ++i) {
		if (args[i] == "--help") {
			writeln(helpText);
			exit(0);
		}
		else if (args[i].skipOver("--num-threads=")) {
			numThreads = args[i].parse!uint;
		}
		else if (args[i].skipOver("--timeout=")) {
			timeout = args[i].parse!uint;
		}
		else if (args[i].skipOver("--tcp-port=")) {
			portno = args[i].parse!ushort;
		}
		else if (args[i] == "--verbose") {
			verbose = true;
		}
	}

	/* initialization */
	signal(SIGINT, &sighandler);
	signal(SIGALRM, &sighandler);

	foreach (ref num; iNumbers)
		num = uniform(0, int.max);

	try {
		/* business logic */
		if (timeout > 0)
			alarm(timeout);

		if (numThreads == 0)
			numThreads = get_nprocs();

		log(verbose, "Spawn ", numThreads, " threads");
		Tid[] tids;
		foreach (_; 0 .. numThreads)
			tids ~= spawn(&tunableLoad);

		sockServer(portno, tids, verbose);
	}
	catch (Exception ex) {
		/* quit */
		writeln("--");
		exit(0);
	}
}


/* data types for pattern matching */
enum LoadDelay { _ };
enum LoadAccessPattern { _ };
enum LoadStatus { _ };

/*
 * perf.:    HIGH ....................... LOW
 * delay:    0 50 100 200 300 400 500 600 700
 * scramble: no ......................... yes
 */
void tunableLoad()
{
	uint delay = 400;
	bool scramble = true;

	while (true) {

		receiveTimeout(dur!"seconds"(0),
			(LoadDelay _, uint d) { delay = d; },
			(LoadAccessPattern _, bool flag) { scramble = flag; },
			(LoadStatus _, Tid tid) { tid.send(delay, scramble); },
		);

		for (int i = 1; i < iNumbers.length; ++i) {

			int j = !scramble ? (i - 1) : cast(int)uniform(0, iNumbers.length);
			int k = !scramble ? i       : cast(int)uniform(0, iNumbers.length);

			iAns = iNumbers[j] + iNumbers[k];
		}

		usleep(delay);
	}
}


void sockServer(ushort portno, Tid[] tids, bool verbose = false)
{
	auto server = new SocketServer(portno);
	auto buff = new ubyte[24];

	while (true) {
		string command;
		auto trans = server.byChunk(buff);
		/*
		 * foreach() finishes upon connection closure,
		 * so sending back is possible only from within foreach().
		 */
		foreach (msg; trans) {
			command ~= cast(string)msg;

			if (command.startsWith("status")) {
				tids[0].send(LoadStatus._, thisTid);
				receive(
					(uint delay, bool scrambled) { trans.put(delay.to!string ~ " " ~ (scrambled ? "y" : "n")); }
				);
			}
		}

		string[] args = command.split;

		switch (args[0]) {
		case "quit":
			throw new Exception("Quit");

		case "delay":
			uint delay = args[1].parse!uint;
			foreach (tid; tids)
				tid.send(LoadDelay._, delay);
			log(verbose, "delay: ", delay);
			break;

		case "access-pattern":
			bool scrambled = (args[1] == "y");
			foreach (tid; tids)
				tid.send(LoadAccessPattern._, scrambled);
			log(verbose, "access-pattern: ", args[1]);
			break;

		case "status":
			/* nothing */
			break;

		default:
			writeln("Unrecognized command: ", command);
		}
	}
}

class SocketServer
{
	import std.socket;

	this(ushort port)
	{
		auto address = new InternetAddress(InternetAddress.ADDR_ANY, port);
		
		m_socket = new TcpSocket();
		m_socket.setOption(SocketOptionLevel.SOCKET, SocketOption.REUSEADDR, true);
		m_socket.bind(address);
		m_socket.listen(1);
	}
	
	auto byChunk(ubyte[] buff)
	{
		return ByChunk(m_socket, buff);
	}
	
	private static struct ByChunk
	{
		this(Socket socket, ubyte[] buff)
		{
			m_newSocket = socket.accept();
			m_buff = buff;
			popFront();
		}
		
		~this()
		{
			destroy();
		}
		
		void destroy()
		{
			m_newSocket.shutdown(SocketShutdown.BOTH);
			m_newSocket.close();
		}
		
		@property
		ubyte[] front()
		{
			return m_front;
		}

		void popFront()
		{
			auto len = m_newSocket.receive(m_buff);
			switch (len) {
			case Socket.ERROR:
			case 0:	/* connection closed */
				m_empty = true;
				m_front.length = 0;
				break;
			default:
				m_empty = false;
				m_front = m_buff[0 .. len];
				break;
			}
		}

		@property
		bool empty()
		{
			return m_empty;
		}

		void put(T)(T data)
		{
			m_newSocket.send(data);
		}

		private Socket m_newSocket;
		private ubyte[] m_buff;
		private ubyte[] m_front;
		private bool m_empty;
	}
	
	private Socket m_socket;
}

