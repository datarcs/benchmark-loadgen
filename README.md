# Loadgen: synthetic load generator for DatArcs Optimizer

## Compilation
This software is written in [the D programming language](http://dlang.org/).

### Compilation command line
```
$ dmd loadgen.d -ofloadgen
```

### Dependencies
- netcat (nc)

## Principle of operation
Loadgen is a synthetic load generator controlled by _application knobs_ via TCP
socket.

### Available knobs
#### delay
This knob controls the number of micro-seconds to suspend the busy loop of _useless calculations_.  
The lower the delay value, the higher the anticipated performance.

### access-pattern
This knob controls the access pattern to the data used for the _useless calculations_.
The access pattern is associated with cache friendliness - "y" value causes
randomization of data access (leading to performance degradation), "n" value
causes sequential data access.

